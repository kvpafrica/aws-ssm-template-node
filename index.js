const awsSsm = require('./lib/aws-ssm');

const KeyRegex = /{{\s*key\s+\\?"([a-zA-Z0-9/_-]+)\\?"\s*}}/ig;
const KeyOrDefaultRegex = /{{\s*key_or_default\s+\\?"([a-zA-Z0-9/_-]+)\\?"\s+\\?"([^\\]*)\\?"\s*}}/ig;
const MAX_KEYS_PER_REQUEST = 10;

const NODE_ENV = process.env.NODE_ENV || 'staging';
const NODE_ENV_REGEX = /_NODE_ENV_/ig;

class ContentProcessor {
  constructor(content, decrypt = false, keyPrefix = "") {
    this.lines = content.split(/[\r\n]/);
    this.keysMatched = {};
    this.md5 = null;
    this.decrypt = decrypt;
    this.keyNs = keyPrefix;
  }


  addKey(key, line, literal, fallback = null) {
    if (!key.startsWith('/')) {
      key = `${this.keyNs}/${key}`;
    }

    if (NODE_ENV_REGEX.test(key)) {
      key = key.replace(NODE_ENV_REGEX, NODE_ENV);
    }

    if (!this.keysMatched.hasOwnProperty(key)) {
      this.keysMatched[key] = [];
    }

    this.keysMatched[key].push({line, literal, fallback});
  }

  async process(listKeysOnly = false) {
    this.lines.forEach((text, lineNo) => {
      let match, match2;
      while ((match = KeyRegex.exec(text)) !== null) {
        this.addKey(match[1], lineNo, match[0]);
      }

      while ((match2 = KeyOrDefaultRegex.exec(text)) !== null) {
        this.addKey(match2[1], lineNo, match2[0], match2[2]);
      }

    });

    if (listKeysOnly) {
      return Object.keys(this.keysMatched);
    }

    await this.resolveKeyValues();
    return this.lines.join('\n');
  }

  async resolveKeyValues() {
    let keys = Object.keys(this.keysMatched);
    console.log('Number of SSM keys found:', keys.length);
    if (keys.length < 1) {
      return null;
    }

    for (let i = 0; i < keys.length; i += MAX_KEYS_PER_REQUEST) {
      let keySlice = keys.slice(i, i + MAX_KEYS_PER_REQUEST);
      let keyValues = await awsSsm.getParametersBatch(keySlice, this.decrypt);
      console.log(`Processing keys: ${i} to ${i + MAX_KEYS_PER_REQUEST}`);
      for (let key in keyValues) {
        if (keyValues.hasOwnProperty(key)) {
          this.resolveKeyValue(key, keyValues[key]);
        }
      }
    }

  }

  resolveKeyValue(key, value) {
    let matches = this.keysMatched[key];
    matches.forEach((lineData) => {
      if (value === null && lineData.fallback === null) {
        throw new Error(`Could not resolve value for key ${key}`);
      }

      value = value || lineData.fallback;
      this.lines[lineData.line] = this.lines[lineData.line].replace(lineData.literal, value);
    });
  }
}


/**
 * Process file content and replace SSM  keys. Using the key / key default format.
 * @param content
 * @param prefix
 * @param withDecryption
 * @return {Promise<*>}
 */
module.exports.processContent = async (content, prefix = "", withDecryption = false) => {
  return new ContentProcessor(content, withDecryption, prefix).process();
};


module.exports.extractKeys = async (content, prefix = "") => {
  return new ContentProcessor(content, false, prefix).process(true);
};