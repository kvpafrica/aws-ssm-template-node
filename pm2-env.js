const fs = require('fs');
const processor = require('.');

const SSM_VALUE_TEST = /^ssm:/i;


const run = async function (args) {
  let localEnv = {};

  for (let i in process.env) {
    if (process.env.hasOwnProperty(i) && SSM_VALUE_TEST.test(process.env[i])) {
      let envValue = process.env[i].replace(SSM_VALUE_TEST, '');
      let defaultValue = null;
      if (envValue.indexOf(':') > 0) {
        let tmp = envValue.split(':', 2);
        envValue = tmp[0];
        defaultValue = tmp[1];
      }

      localEnv[i] = defaultValue === null
        ? `{{key "${envValue}"}}`
        : `{{key_or_default "${envValue}" "${defaultValue}"}}`;
    }
  }

  let appConfig = {
    name: args.app_name,
    script: fs.realpathSync(args.script),
    watch: args.watch,
    env: localEnv,
    instances: args.instances || parseInt(process.env.CONCURRENCY) || 'max',
    max_restarts: 10
  };

  let content = JSON.stringify(appConfig, null, 2);

  if (args.keys) {
    return processor.extractKeys(content, args.key_prefix);
  }

  return processor.processContent(content, args.key_prefix, args.decrypt);
};

//start application
const ArgumentParser = require('argparse').ArgumentParser;

const parser = new ArgumentParser({
  version: '1.0.0',
  addHelp: true,
  description: 'PM2 App generator'
});


parser.addArgument(
  ['--keys'],
  {
    help: 'Specifies if only the list of keys should be returned.',
    type: Boolean,
    defaultValue: false
  }
);

parser.addArgument(
  ['--watch'],
  {
    help: 'Specifies if the application should be watched',
    type: Boolean,
    defaultValue: false
  }
);

parser.addArgument(
  ['--instances'],
  {
    help: 'Specifies number of instances to fork. Defaults to env variable, CONCURRENCY or "max" if that is not available',
    type: Number
  }
);

parser.addArgument(
  ['-o', '--output'],
  {
    help: 'Output file, echo to CLI if not available',
    defaultValue: ''
  }
);

parser.addArgument(
  ['-s', '--script'],
  {
    help: 'Name of the script to execute',
    defaultValue: './index.js'
  }
);

parser.addArgument(
  ['--app-name'],
  {
    help: 'Application name to fetch',
    defaultValue: 'app'
  }
);

parser.addArgument(
  ['--key-prefix'],
  {
    help: 'Prefix of SSM keys',
    defaultValue: ''
  }
);


parser.addArgument(
  ['--decrypt'],
  {
    help: 'Specifies if the key values should be decrypted.',
    type: Boolean
  }
);

let args = parser.parseArgs();

run(args)
  .then((result) => {
    if (Array.isArray(result)) {
      console.log('====Keys Matched=====');
      result.forEach((k) => console.log(k));
      console.log('====Keys Matched=====');
    } else {
      if (args.output) {
        fs.writeFileSync(args.output, new Buffer(result), {
          encoding: 'utf8'
        });
      } else {
        console.log(result);
      }
    }
  })
  .catch((err) => {
    console.error('Failed: ', err);
    process.exit(1);
  });
