'use strict';
const request = require('request');
const templateProcessor = require('.');
const yamlJson = require('yamljs');
const ArgumentParser = require('argparse').ArgumentParser;
const fs = require('fs');
const md5 = require('./lib/md5-sum');

const RequestMd5SumCache = {};


const getConfig = (args) => {
  return new Promise((resolve, reject) => {
    request(`${args.server_url}/${args.app_name}.yaml`, {
      auth: {
        user: `${args.username}`,
        pass: `${args.password}`
      },
      json: false
    }, (err, res, body) => {
      if (err) {
        reject(err);
      } else {
        if (res.statusCode !== 200) {
          reject(new Error(`Status Code - ${res.statusCode} -> ${body}`))
        } else {
          resolve(body);
        }

      }
    });
  });
};

const handleProcess = async (args) => {
  //get content
  let content = await getConfig(args);

  //process content
  let prefix = args.key_prefix || "";

  if (args.keys) {
    return await templateProcessor.extractKeys(content, prefix);
  }

  let cacheKey = `${args.server_url}/${args.app_name}`;
  let contentMd5 = md5.text(content);
  if (!args.force_reload && RequestMd5SumCache.hasOwnProperty(cacheKey)) {
    let cachedObject = RequestMd5SumCache[cacheKey];
    console.log('Cached Content MD5: ', cachedObject.contentMd5, 'New content MD5', contentMd5);
    if (cachedObject.contentMd5 === contentMd5) {
      console.log('Returning cached result');
      return cachedObject.processResult;
    }
  }

  let intermediateResult = await templateProcessor.processContent(content, prefix, args.decrypt);

  //convert to JSON
  let config = yamlJson.parse(intermediateResult);
  let processResult = JSON.stringify(config, null, 2);
  RequestMd5SumCache[cacheKey] = {
    contentMd5,
    processResult
  };

  return processResult;
};

function run(args) {
  handleProcess(args).then((content) => {
    if (args.keys) {
      console.log("=====Keys listed below====");
      content.forEach((k) => console.log(k));
      return;
    }

    if (!args.output) {
      console.log(content);
    } else {
      let filePath = args.output;
      try {
        let md5Exist = null;
        if (fs.existsSync(filePath)) {
          md5Exist = md5.textFile(filePath);
        }
        let md5New = md5.text(content);
        console.log(`MD5 Sum : Existing => ${md5Exist}, New => ${md5New}`);
        if (md5New !== md5Exist) {
          console.log(filePath, 'is being updated.');
          fs.writeFileSync(filePath, new Buffer(content), {
            encoding: 'utf8'
          });
        } else {
          console.log('No updated needed');
        }
      } catch (err) {
        console.error(err);
        if (args.interval < 1) {
          process.exit(1);
        }
      }
    }
  })
    .catch((err) => {
      console.error('Failed: ', err);
      if (args.interval < 1) {
        process.exit(1);
      }
    });
}


///start processing:

const parser = new ArgumentParser({
  version: '0.1.0',
  addHelp: true,
  description: 'Config Server generator'
});

parser.addArgument(
  ['-l', '--server-url'],
  {
    help: 'Spring Config Server URL',
    required: true
  }
);

parser.addArgument(
  ['-u', '--username'],
  {
    help: 'Config Server username'
  }
);

parser.addArgument(
  ['-i', '--interval'],
  {
    help: 'Config refresh interval. If specified, application will continue till terminated',
    defaultValue: 0,
    type: Number
  }
);

parser.addArgument(
  ['-p', '--password'],
  {
    help: 'Config Server password'
  }
);

parser.addArgument(
  ['-o', '--output'],
  {
    help: 'Output file, echo to CLI if not available'
  }
);

parser.addArgument(
  ['--app-name'],
  {
    help: 'Application name to fetch',
    defaultValue: 'application'
  }
);

parser.addArgument(
  ['--key-prefix'],
  {
    help: 'Prefix of SSM keys',
    defaultValue: ''
  }
);

parser.addArgument(
  ['--keys'],
  {
    help: 'Specifies if only the list of keys should be returned.',
    type: Boolean,
    defaultValue: false
  }
);

parser.addArgument(
  ['-f', '--force-reload'],
  {
    help: 'Specifies if reload should be forced',
    type: Boolean,
    defaultValue: false
  }
);

parser.addArgument(
  ['--decrypt'],
  {
    help: 'Specifies if the key values should be decrypted.',
    type: Boolean
  }
);

let args = parser.parseArgs();

if (args.interval > 0) {
  run(args);
  console.dir("Setting up interval");
  setInterval(() => {
    console.warn("Refreshing config.");
    run(args);
  }, args.interval)
} else {
  run(args);
}



