# SSM Template Generator

This is a collection of libraries written in Node.js that can be used to load generate configurations for node apps or any other runtime using SSM key values.

## Default Processor

The default content processor will load a content and replace every line with SSM keys with their equivalent value.


**Usage**

```javascript
const processor = require('aws-ssm-template-node');

let content = require('fs').readFileSync('/path/to/file', {encoding: 'utf8'}); //or load from some other source
//get SSM keys to be processed:
processor.extractKeys(content, '')
.then((keyResult) => {
  //do anything with keys
});

//replace keys with SSM value, decrypt if encypted
processor.processContent(content, '', true)
.then((contentResult) => {
  //do anything with processed content
});

```

**Content Placeholders**
- `{{key "key-name" }}` specifies that the place holder be replaced with SSM key `key-name`, the rules for processing keys are obeyed.
- `{{key_or_default "key-name" "defaule-value"}}` similar to the `key` placeholder, but this also specifies a default value. Please note that default value cannot contain `'\'`.

*sample.yml*
```
data: 
  value: '{{key "value/foo/bar"}}'
  user: '{{key "value/foo/baz"}}'
  pass: '{{key_or_default "value/foo/baz" "fallback"}}'
  url: '{{key_or_default "/production/value/foo/baz" "fallback"}}'
  process: '{{key "/_NODE_ENV_/value/foo/baz"}}'
  
```

The following rules are followed when extracting SSM keys from the raw content. 

- Keys that start with `'/'` are assumed to be absolute
- Keys that do not start with `'/`' are prefixed with the specified key prefix, please note that if key-prefix is missing, they are prefixed with `'/''`.
- For keys containing `_NODE_ENV_`, the part is replaced with `process.env.NODE_ENV`, this defaults to `production`    
- Key prefix can contain `_NODE_ENV_`

For the `sample.yml` file above, assuming the key prefix is `/_NODE_ENV_/app`, then the following keys will be resolved, for `NODE_ENV=production`:


*sample.yml resolved*

```
data: 
  value: '{{key "value/foo/bar"}}' # /production/app/value/foo/bar
  user: '{{key "value/foo/baz"}}'  # /production/app/value/foo/baz
  pass: '{{key_or_default "value/foo/baz2" "fallback"}}' # /production/app/value/foo/baz2
  url: '{{key_or_default "/production/value/foo/baz" "fallback"}}' # /production/value/foo/baz resolves to 'fallback' if key is missing
  process: '{{key "/_NODE_ENV_/value/foo/baz"}}' # /production/value/foo/baz
```


## Spring Config Server Generator

This script loads specified config file from a Spring config server, replaces the SSM placeholders with the found SSM key values and then generates a JSON file as output.

### Usage

Installation using `npm -i -g`
```text
#For 
config-server [-h] [-v] -l SERVER_URL [-u USERNAME] [-i INTERVAL]
                     [-p PASSWORD] [-o OUTPUT] [--app-name APP_NAME]
                     [--key-prefix KEY_PREFIX] [--keys KEYS] [-f FORCE_RELOAD]
                     [--decrypt DECRYPT]

```

Regular Node Installation:

```text
./bin/config-server [-h] [-v] -l SERVER_URL [-u USERNAME] [-i INTERVAL]
                     [-p PASSWORD] [-o OUTPUT] [--app-name APP_NAME]
                     [--key-prefix KEY_PREFIX] [--keys KEYS] [-f FORCE_RELOAD]
                     [--decrypt DECRYPT]
```

**Available Options**

```text

Optional arguments:
  -h, --help            Show help message and exit.
  -v, --version         Show program's version number and exit.
  -l SERVER_URL, --server-url SERVER_URL
                        Spring Config Server URL
  -u USERNAME, --username USERNAME
                        Config Server username
  -i INTERVAL, --interval INTERVAL
                        Config refresh interval. If specified, application 
                        will continue till terminated
  -p PASSWORD, --password PASSWORD
                        Config Server password
  -o OUTPUT, --output OUTPUT
                        Output file, echo to CLI if not available
  --app-name APP_NAME   Application name to fetch
  --key-prefix KEY_PREFIX
                        Prefix of SSM keys
  --keys KEYS           Specifies if only the list of keys should be returned.
  -f FORCE_RELOAD, --force-reload FORCE_RELOAD
                        Specifies if reload should be forced
  --decrypt DECRYPT     Specifies if the key values should be decrypted.

```



## PM2 Process File Generator

This generates a [PM2 process file](http://pm2.keymetrics.io/docs/usage/application-declaration/) for the specified script file. The script converts all the environment variable starting `ssm:` to their matched SSM keys and generates a PM2 file. Environment Variable values should follow the following pattern: `ssm:key-name` or `ssm:key-name:defaultValue` 

### Usage

**Node Global Installation**

```bash
pm2loader [-h] [-v] [--keys KEYS] [-o OUTPUT] [-s SCRIPT]
                 [--app-name APP_NAME] [--key-prefix KEY_PREFIX]
                 [--decrypt DECRYPT]

```

**Node Package**

```bash
./bin/pm2loader [-h] [-v] [--keys KEYS] [-o OUTPUT] [-s SCRIPT]
                 [--app-name APP_NAME] [--key-prefix KEY_PREFIX]
                 [--decrypt DECRYPT]

```

**CLI Options**

```text
  -h, --help            Show help message and exit.
  -v, --version         Show program's version number and exit.
  --keys KEYS           Specifies if only the list of keys should be returned.
  -o OUTPUT, --output OUTPUT
                        Output file, echo to CLI if not available
  -s SCRIPT, --script SCRIPT
                        Name of the script to execute
  --app-name APP_NAME   Application name to fetch
  --key-prefix KEY_PREFIX
                        Prefix of SSM keys
  --decrypt DECRYPT     Specifies if the key values should be decrypted.

```

**Example**

For example, given the following environment file:

```text
export DB_PASSWORD='ssm:/production/app/db/password'
export DB_USERNAME='ssm:/production/app/db/username:user'
export API_USERNAME='ssm:app/apiKey:api-key'
```

```
pm2loader -o app.json -s './index.js' --key-prefix /_NODE_ENV_
```

The following JSON file is generated in the intermediate:

```json
{
  "name": "app",
  "script": "/Users/taiwo/development/library/aws-ssm-template-node/index.js",
  "watch": true,
  "env": {
    "DB_PASSWORD": "{{key \"/production/app/db/password\"}}",
    "API_USERNAME": "{{key_or_default \"app/apiKey\" \"api-key\"}}",
    "DB_USERNAME": "{{key_or_default \"/production/app/db/username\" \"user\"}}"
  },
  "instances": "max",
  "max_restarts": 10
}
```

The resulting JSON file is passed through the content processor and the output is saved in the specified output file. 


## Using Outside AWS or with None IAM Instances

See the [AWS environment variable](https://docs.aws.amazon.com/sdk-for-javascript/v2/developer-guide/loading-node-credentials-environment.html) page for environment variables that need to be set.

