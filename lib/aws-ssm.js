const AWS = require('aws-sdk');
const SSM = new AWS.SSM({
  region: 'us-west-2'
});


module.exports.getParametersBatch = (keys, decrypt) => {
  const params = {
    Names: keys,
    WithDecryption: decrypt
  };


  return new Promise(function (resolve, reject) {
    SSM.getParameters(params, function (err, data) {
      if (err) {
        return reject(err);
      }

      let result = {};
      data.Parameters.forEach(parameter => {
        result[parameter.Name] = parameter.Value;
      });

      data.InvalidParameters.forEach((d) => result[d] = null);
      resolve(result);
    });
  });
};