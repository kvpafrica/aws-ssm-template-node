const crypto = require('crypto');
const fs = require('fs');

module.exports.text = function (str) {
  let cr = crypto.createHash('md5');
  cr.update(str);
  return cr.digest('hex');
};


module.exports.textFile = function (filePath, encoding = 'utf8') {
  let content = fs.readFileSync(filePath, {encoding});
  return module.exports.text(content);
};
